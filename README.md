AR-netsuite-standards
=========

NetSuite Machine Hardware and Software standards retrieval and validation tasks go here. Take care to distinguish netsuite only standards. Windows standards or other pos standards should reside elsewhere.

Requirements
------------

JSON Schema Version : {{ Insert Value Here }}
WINRM Configurations

Role Variables
--------------

Fact subsets (Hardware, Software, Functional Software sets)

Dependencies
------------

Example Playbook
----------------

License
-------

BSD

Author Information
------------------
